import dnspython as dns
# from dns import resolver
import dns.resolver
# from dns import resolver
import socket
import time
import geocoder
import threading
from socket import *
from queue import Queue

#validating ip address:

def validIPAddress(IP):
    def isIPv4(s):
        try: return str(int(s)) == s and 0 <= int(s) <= 255
        except: return False
    def isIPv6(s):
        if len(s) > 4:
            return False
        try : 
            return int(s, 16) >= 0 and s[0] != '-'
        except:
            return False
        if IP.scount(".") == 3 and all(isIPv4(i) for i in IP.split(".")):
            return "IPv4"
        if IP.count(":") == 7 and all(isIPv6(i) for i in IP.split(":")):
            return "IPv6"
        return "Neither"


domainname=input("Enter a domain name that you want to search:")
#getting ipaddress of the domain
ip=gethostbyname(domainname)

def main():
    if(validIPAddress(ip)=="Neither"):
        print("not a proper ip address")
    else:
        print("The given domain belongs to address ",validIPAddress(ip))
        print("IP address of the domain is:",ip)
        # A record 
        try:
            result = dns.resolver.resolve(domainname, 'A')
            print(" ")
            for val in result:
                print('A Record : ', val.to_text())
            #ptr record
            ptr= gethostbyaddr(ip)
            print("ptr record:",ptr)
            print(" ")
        except:
            print("No A records for",domainname)
        #AAA record
        try:
            aaarec=dns.resolver.resolve(domainname, 'AAAA')

            print("AAA record")
            
            for val in aaarec:
                print("AAA record:",val.to_text())
        except:
            print("No AAA records for",domainname)
            
        #ns(nameserver) record
        print(" ")
        try:
            print("NS records")
            
            ns = dns.resolver.resolve(domainname, 'NS')
            # Printing record
            for val in ns:
                print('NS Record : ', val.to_text())
        except:
            print("No NS records for",domainname)

    #mx record
        print(" ")
        try:
            print("MX records")

            mx = dns.resolver.resolve(domainname, 'MX')
            # Printing record
            for val in mx:
                print('MX Record : ', val.to_text())
        except:
            print("No MX records for",domainname)
    #soa record
        print(" ")
        try:
            print("SOA Records")
            soa = dns.resolver.resolve(domainname, 'SOA')
            # Printing record
            for val in soa:
                print('SOA Record : ', val.to_text())
        except:
            print("No SOA records for",domainname)
            
            
    #Geolocation
        print(" ")
        ip = geocoder.ip(gethostbyname(domainname))
        print("Geo Location")
        print("Country:",ip.country)
        print("State:",ip.state)
        print("City:",ip.city)
        print("Coordinates:",ip.latlng)
        print(" ")
        
    #Ports info
        setdefaulttimeout(0.25)
        print_lock = threading.Lock()
        target=domainname
        t_IP = gethostbyname(target)
        print ('Starting scan on host: ', t_IP)

        def portscan(port):
            s = socket(AF_INET,SOCK_STREAM)
            try:
                con = s.connect((t_IP, port))
                with print_lock:
                    print(port, 'is open')
                con.close()
            except:
                pass

        def threader():
            while True:
                worker = q.get()
                portscan(worker)
                q.task_done()
                
        q = Queue()
        startTime = time.time()
        
        for x in range(10000):
            t = threading.Thread(target = threader)
            t.daemon = True
            t.start()
        
        for worker in range(1, 500):
            q.put(worker)
        
        q.join()
        print('Time taken:', time.time() - startTime)