import requests
import re
import json
import configparser

config=configparser.ConfigParser()
config.read('config.ini')
apikey=config['hunterio']['Api_key']
# print(type(apikey))
def domain(key):
	
	domain1=key
	domain_url=f"https://api.hunter.io/v2/domain-search?domain={domain1}&api_key={apikey}"
	res_d=requests.get(domain_url)
	data=res_d.json()
	# data_dict= json.loads(data)
	# if(data[data])
	# sorting=json.dumps(data,indent=4)
	# print(data)
	if(data['data']['organization']==None):
		# print("Invalid Domain")
		pass
	else:
		return True
# domain("google.com")
def finder(key,f_name,l_name):
	# find_domain=domain(key)
	first_name=f_name
	last_name=l_name
	find_url=f"https://api.hunter.io/v2/email-finder?domain={key}&first_name={first_name}&last_name={last_name}&api_key={apikey}"
	finder_res=requests.get(find_url)
	find_json=finder_res.json()
	find_prettify=json.dumps(find_json,indent=4)
	return find_prettify

def breach(email):
	breach= "https://breachdirectory.p.rapidapi.com/"
	querystring = {"func":"email","term":"varakumar7000@gmail.com"}
	headers = {
		"X-RapidAPI-Host": "breachdirectory.p.rapidapi.com",
		"X-RapidAPI-Key": config['breach']['breach_api']
	}
	response_b = requests.request("GET", breach, headers=headers, params=email)
	pretty=response_b.content
	pretty=pretty.decode('utf8')
	pretty=pretty.replace("\n","")
	# print(type(pretty))
	out = json.loads(pretty)
	out=json.dumps(pretty,indent=4,sort_keys=True)
	return out
def verify(email):
	domain_verification=f"https://api.hunter.io/v2/email-verifier?email={email}&api_key={apikey}"
	response=requests.get(domain_verification)
	res=response.content.decode('UTF-8')  
	return res
def verifier(email):
	# regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'  
	regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]'
	if(re.search(regex,email)):
		return True  
	else:
		return False