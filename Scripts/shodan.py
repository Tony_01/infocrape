import requests
import configparser
import json
import re
import subprocess
import platform
import os
import threading

config=configparser.ConfigParser()
config.read('config.ini')
shodankey=config['shodan']['shodan_api']

def ports(ip):
    try:
        p = subprocess.run(f"nmap -p 1-1000 {ip} -oN nmap_out.txt",shell=True, stdout=subprocess.PIPE)
        content = f.read()
        f.close()
        nmap=json.dumps(content, indent=4, sort_keys=True)
    except:
        nmap="Nmap not installed properly"
    return nmap
    
def checkip(ip):
    regex = "^((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])$"
    if(re.search(regex, ip)):
        return "Valid IP"
    else:
        return "Invalid IP"
def alt_ip(res):
    global data
    data=res
    
def ip(ip):
    url=f"https://api.shodan.io/shodan/host/{ip}?key={shodankey}"
    response=requests.get(url)
    response=response.json()
    response=json.dumps(response,indent=4)
    # print(response)
    return alt_ip(response)
# ports("142.250.195.174")