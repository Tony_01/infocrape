from tkinter import *
import platform
import os
import main
# import torch
from tkinter import messagebox

def accept(inf):
    data_accept=Label(root,text=inf)
    data_accept.place(x=170,y=200)

def data(event):
    key=str(search.get())
    
    if not key:
        messagebox.showerror("Infoscrape", "Input Field is Empty").config(font=('courier',10))
    else:
        top=Toplevel(root)
        top.geometry("500x400")
        T = Text(top, height = 100, width = 300)
        T.pack()
        top.lift()
        top.grab_set()
        def on_closing():
            search.delete(0,END)
            search.insert(0,"")
            top.destroy()
        top.protocol("WM_DELETE_WINDOW", on_closing)
        check=main.run(key)
        if(check[0]=='Breach'):
            T.insert(END, "[+]Breach\n")
            try:
                breach=check[1]
            except:
                breach=print("No breach data")

            T.insert(END, "\n[+]Checking Email\n")
            try:
                verify_email=check[2]
            except:
                verify_email=print("No Email")
            T.insert(END,breach)
            T.insert(END,"\n")
            T.insert(END,verify_email)
        elif(check[0]=="Find_Email"):
            T.insert(END, "[+]Finding Email")
            finding_email=check[1]
            T.insert(END,finding_email)
        elif(check[0]=="Ip"):
            T.insert(END, "\n[+]Ports\n")
            try:
                ip=check[1]
                ip2=check[2]
            except:
                ip="NO data for this Ip"
            top.geometry("800x600")
            T.insert(END,ip2)
            T.insert(END, "[+]IP scanning\n")
            T.insert(END,ip)
        elif(check[0]=="Social_data"):
            T.insert(END,check[1])
            
        T.config(state="disabled")
        top.mainloop()

def sub():
    if(platform.system()=="Linux"):
        os.system("gedit config.ini")
    else:
        os.system("notepad config.ini")

root=Tk()
app_width=400
app_height=300
screen_width=root.winfo_screenwidth()
screen_height=root.winfo_screenheight()
x=(screen_width/2)-(app_width/2)
y=(screen_height/2)-(app_height/2)
root.geometry(f"{app_width}x{app_height}+{int(x)}+{int(y)}")
root.title("Infoscrape")
root.resizable(0,0)
search_label=Label(root,text="Enter Domain,Email or an IP")
search=Entry(root,width=30)
Search_button=Button(root,text="Search",command=lambda:data(None))
root.bind('<Return>',data)
config=Button(root,text="Config",font=('courier',10),command=sub)
#place
search.place(x=90,y=100)
Search_button.place(x=170,y=130)
search_label.place(x=100,y=80)
config.place(x=10,y=10)
search.focus()
#config
search_label.config(font=('courier',10))
Search_button.config(font=('courier',10))
search.config(font=('courier',10))

root.mainloop()