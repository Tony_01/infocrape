#from numba import jit
import email_osint as e
from tkinter import messagebox
import shodan as s
import threading
import subprocess
import socket

def read_file():
	with open(r"nmap_out.txt", 'r+') as fp:
		lines = fp.readlines()
		fp.seek(0)
		fp.truncate()
		hey=fp.writelines(lines[2:-1])
#@jit()
def run(key):
	upd_key=key.split(" ")
	if(len(upd_key)==3):
		if(e.domain(upd_key[0])==True):
			find_=e.finder(upd_key[0], upd_key[1] ,upd_key[2])
			return ["Find_Email",find_]
		else:
			messagebox.showerror('Arguements Error', 'Invalid No of Inputs Given')
			exit()
	elif(len(upd_key)==1):
		if (e.verifier(key)==True):
			Breach=e.breach(key)
			verify_email=e.verify(key)
			return ["Breach",Breach,verify_email]
		elif(s.checkip(key)=="Valid IP"):
			thread1=threading.Thread(target=s.ip,args=(key,))
			thread1.start()
			thread2=threading.Thread(target=s.ports,args=(key,))
			thread2.start()
			thread1.join()
			thread2.join()
			ip=s.data
			read_file()
			with open("nmap_out.txt",'rb') as f:
				nmap_ports=f.read()
			return ["Ip",ip,nmap_ports]
		elif(e.domain(key)==True):
			convert=socket.gethostbyname('google.com')
			thread1=threading.Thread(target=s.ip,args=(convert,))
			thread1.start()
			thread2=threading.Thread(target=s.ports,args=(convert,))
			thread2.start()
			thread1.join()
			thread2.join()
			ip=s.data
			read_file()
			with open("nmap_out.txt",'rb') as f:
				nmap_ports=f.read()
			# print(nmap_ports)
			return ["Ip",ip,nmap_ports]

		else:
			p = subprocess.run(f"python3 social-media\scrape.py {key}",shell=True, stdout=subprocess.PIPE,text=True)
			return ["Social_data",p.stdout]
	else:
		messagebox.showerror('Arguements Error', 'Invalid No of Inputs Given')
		exit()
